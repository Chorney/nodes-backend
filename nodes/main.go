package main

import (
	"fmt"
	"log"
	"net/http"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/controllers"

	"github.com/gorilla/mux"
)

func main() {

	fmt.Println("Production Mode: ", config.Options.ProductionFlag)
	fmt.Println("Template Mode: ", config.Options.TemplateMode)

	err := http.ListenAndServe(":8085", handlers())
	if err != nil {
		log.Fatal(err)
	}
}

func handlers() http.Handler {
	ac := controllers.NewAssetController()
	lc := controllers.NewLinkController()
	sc := controllers.NewSessionController()

	r := mux.NewRouter()
	r.HandleFunc("/api/assets", ac.Index).Methods("GET")

	//assets
	if config.Options.TemplateMode == "true" {
		//forms
		r.HandleFunc("/api/assets/create", ac.CreateForm).Methods("GET")
		r.HandleFunc("/api/assets/update/{id}", ac.UpdateForm).Methods("GET")

		r.HandleFunc("/api/assets/create", ac.Create).Methods("POST")

		r.HandleFunc("/api/assets/update/{id}", ac.Update).Methods("POST")
		r.HandleFunc("/api/assets/delete/{id}", ac.Delete).Methods("GET")
	} else {
		r.HandleFunc("/api/assets", ac.Create).Methods("POST")        //Create
		r.HandleFunc("/api/assets/{id}", ac.Show).Methods("GET")      //Show
		r.HandleFunc("/api/assets/{id}", ac.Update).Methods("PATCH")  //UPdate
		r.HandleFunc("/api/assets/{id}", ac.Delete).Methods("DELETE") //DELeTE
	}

	r.HandleFunc("/api/links", MakeUserLockedHandler(lc.Index)).Methods("GET")
	r.HandleFunc("/api/links/node/{id}", MakeUserLockedHandler(lc.GetLinksForNode)).Methods("GET") //Show

	// //links
	if config.Options.TemplateMode == "true" {
		//forms
		r.HandleFunc("/api/links/create", lc.CreateForm).Methods("GET")
		r.HandleFunc("/api/links/create", lc.Create).Methods("POST")
		r.HandleFunc("/api/links/delete/{id}", lc.Delete).Methods("GET")
	} else {
		r.HandleFunc("/api/links", MakeUserLockedHandler(lc.Create)).Methods("POST")        //Create
		r.HandleFunc("/api/links/{id}", MakeUserLockedHandler(lc.Delete)).Methods("DELETE") //DELeTE
	}

	//Sessions
	r.HandleFunc("/api/users/session", sc.Create).Methods("POST")                          //Create
	r.HandleFunc("/api/users/session", MakeUserLockedHandler(sc.Delete)).Methods("DELETE") //DELeTE

	return r
}

func MakeUserLockedHandler(fn func(w http.ResponseWriter, r *http.Request)) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if config.Options.UnlockedMode == "false" {
			status, _ := controllers.AlreadyLoggedIn(r)
			if status == false {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}
		fn(w, r)
	}
}
