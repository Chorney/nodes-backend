package main

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRouting(t *testing.T) {

	srv := httptest.NewServer(handlers())
	defer srv.Close()

	res, err := http.Get(fmt.Sprintf("%s/api/assets", srv.URL))
	if err != nil {
		t.Fatalf("could not send Get Request: %v", err)
	}

	if res.StatusCode != http.StatusOK {
		t.Errorf("expected status OK; got %v", res.Status)
	}

}
