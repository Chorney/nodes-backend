package database

import (
	"fmt"
	"projects/lampoon/services/nodes/logging"
	"reflect"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//FILENAME used for error logging
const FILENAME = "mongo.go"

//Model interface that is passed into database functions
type Model interface {
	GetModelName() string
	SetModelID(interface{}) error
	GetModelID() bson.ObjectId
}

type mongoDb struct {
	db *mgo.Database
}

//DialDbDirect contacts the server direct and returns mgo database structure
func DialDbDirect(server string, dbname string) (*mgo.Database, error) {

	//NewMongoDb returns a *mongoDB struct and an error
	s, err := mgo.Dial(server)

	// Check if connection error, is mongo running ?
	if err != nil {
		logging.Critical("Trouble connecting to the mongodb server", FILENAME+"-NewMongoDb", err)
		panic(err)
	}

	if err = s.Ping(); err != nil {
		logging.Critical("Trouble pinging the mongodb server", FILENAME+"-NewMongoDb", err)
		panic(err)
	}

	logging.Notice("You connected to your local mongo database", FILENAME+"-NewMongoDb")

	DB := s.DB(dbname)
	return DB, err
}

//NewMongoDb returns a *mongoDB struct and an error
func NewMongoDb(server string, dbname string) (*mongoDb, error) {
	dbstruct := mongoDb{}

	s, err := mgo.Dial(server)

	// Check if connection error, is mongo running ?
	if err != nil {
		logging.Critical("Trouble connecting to the mongodb server", FILENAME+"-NewMongoDb", err)
		panic(err)
	}

	if err = s.Ping(); err != nil {
		logging.Critical("Trouble pinging the mongodb server", FILENAME+"-NewMongoDb", err)
		panic(err)
	}

	logging.Notice("You connected to your local mongo database", FILENAME+"-NewMongoDb")

	dbstruct.db = s.DB(dbname)

	return &dbstruct, err
}

func (dbl mongoDb) GetDb() *mgo.Database {
	return dbl.db
}

func (dbl mongoDb) SelectAll(m Model) (interface{}, error) {

	// modeltype := reflect.PtrTo(reflect.TypeOf(m))
	modeltype := reflect.TypeOf(m)
	slicetype := reflect.SliceOf(modeltype)

	a := reflect.New(slicetype).Interface()

	err := dbl.db.C(m.GetModelName()).Find(bson.M{}).All(a)
	if err != nil {
		logging.Error("Trouble finding elements in the database", FILENAME+"-SelectAll", err)
	}
	return a, err
}

func (dbl mongoDb) CreateEntry(m Model) (interface{}, error) {

	// b := bson.NewObjectIdWithTime(time.Now())
	id := m.GetModelID().Hex()
	if id == "" {
		b := bson.NewObjectId()
		err := m.SetModelID(b)
		if err != nil {
			msg := "Issues setting the model ID"
			logging.Error(msg, FILENAME+"-CreateEntry", err)
			return b, fmt.Errorf(msg)
		}
	}
	err := dbl.db.C(m.GetModelName()).Insert(m)
	if err != nil {
		msg := "Problem inserting document into database"
		logging.Error(msg, FILENAME+"-CreateEntry", err)
		return m.GetModelID(), fmt.Errorf(msg, err)
	}
	return m.GetModelID(), nil
}

func (dbl mongoDb) DeleteEntry(m Model) error {

	id := m.GetModelID()
	err := dbl.db.C(m.GetModelName()).RemoveId(id)
	if err != nil {
		msg := "Error deleting the document from the database"
		logging.Error(msg, FILENAME+"-DeleteEntry", err)
		return fmt.Errorf(msg, err)
	}
	return nil
}

func (dbl mongoDb) EmptyCollection(m Model) (int, error) {
	info, err := dbl.db.C(m.GetModelName()).RemoveAll(bson.M{})
	if err != nil {
		msg := "Error clearing the model collection from database"
		logging.Error(msg, FILENAME+"-EmptyCollection", err)
		return 0, err
	}
	return info.Removed, nil
}

//list = make(map[string]interface{}
//list["name"] = "Cat"
//list["title"] = "Peasant"
func (dbl mongoDb) FindEntries(m Model, list map[string]interface{}) (interface{}, error) {

	//
	modeltype := reflect.TypeOf(m)
	slicetype := reflect.SliceOf(modeltype)
	a := reflect.New(slicetype).Interface()

	err := dbl.db.C(m.GetModelName()).Find(list).All(a)
	if err != nil {
		msg := fmt.Sprintf("Error finding elements in the database: %+v", list)
		logging.Error(msg, FILENAME+"-FindEntries", err)
		return nil, err
	}
	return a, nil
}

func (dbl mongoDb) FindEntry(m Model) (interface{}, error) {

	//
	modeltype := reflect.TypeOf(m)
	a := reflect.New(modeltype).Interface()

	id := m.GetModelID()
	if id.Hex() == "" {
		return nil, fmt.Errorf("ID not set in the model input")
	}

	err := dbl.db.C(m.GetModelName()).FindId(id).One(a)
	if err != nil {
		msg := fmt.Sprintf("Error retrieving entry in the database: %+v", id)
		logging.Error(msg, FILENAME+"-FindEntry", err)
		return nil, err
	}
	return a, nil
}

func (dbl mongoDb) UpdateEntry(m Model, findlist map[string]interface{}, updates map[string]interface{}) (int, error) {

	if len(updates) == 0 {
		return 0, nil
	}

	ups := bson.M{"$set": updates}

	// fmt.Println(findlist)
	// fmt.Println(ups)

	info, err := dbl.db.C(m.GetModelName()).UpdateAll(findlist, ups)
	if err != nil {
		msg := fmt.Sprintf("Error finding elements in the database: %+v", findlist)
		logging.Error(msg, FILENAME+"-UpdateEntry", err)
	}
	return info.Updated, err
}

func (dbl mongoDb) GetCount(m Model) (int, error) {
	return dbl.db.C(m.GetModelName()).Find(bson.M{}).Count()
}

//DeleteEntries removes the given list of items from the collection
func DeleteEntries(db *mgo.Database, collectionname string, removelist []bson.ObjectId) (int, error) {

	cnt := 0
	for _, id := range removelist {
		err := db.C(collectionname).RemoveId(id)
		if err != nil {
			logging.Error("Unable to delete entry", FILENAME+"-DeleteEntries", err)
		} else {
			cnt++
		}
	}
	return cnt, nil
}
