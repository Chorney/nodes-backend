package database

import (
	"fmt"
	"testing"

	"gopkg.in/mgo.v2/bson"
)

const TESTDATABASE = "testname"

var testconfigservername string = "mongodb://db_nodes"
var testconfigdbname string = "graph"

type TestModel struct {
	Name  string        `json: "name"`
	Title string        `json: "title"`
	ID    bson.ObjectId `json:"id" bson:"_id"`
}

func (tm *TestModel) GetModelName() string {
	name := TESTDATABASE
	return name
}

func (ast *TestModel) SetModelID(id interface{}) error {
	ast.ID = id.(bson.ObjectId)
	return nil
}

func (ast *TestModel) GetModelID() bson.ObjectId {
	return ast.ID
}
func TestNewMongoDb(t *testing.T) {

	//testing to see if connection works manually first
	_, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

}

func TestCreateEntry(t *testing.T) {

	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	//test case 1
	tc := TestModel{"blah", "emperor", bson.NewObjectId()}
	DB.CreateEntry(&tc)

	n, err := DB.db.C(tc.GetModelName()).Find(bson.M{"name": "blah"}).Count()
	if err != nil {
		t.Fatalf("Issues inserting into the data")
	}
	if n == 0 {
		t.Errorf("Issues inserting into the database")
	}

	//test case 2 - empty ID
	tc = TestModel{Name: "test2"}
	DB.CreateEntry(&tc)

	if tc.GetModelID().Hex() == "" {
		t.Errorf("The model ID was not set correctly, should not be ''")
	}

	n, err = DB.db.C(tc.GetModelName()).Find(bson.M{"name": "test2"}).Count()
	if err != nil {
		t.Fatalf("Issues inserting into the data")
	}
	if n == 0 {
		t.Errorf("Issues inserting into the database")
	}
}

func TestGetCount(t *testing.T) {

	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	DB.GetDb().C(TESTDATABASE).DropCollection()

	//item 1
	item1 := TestModel{"blah", "emperor", bson.NewObjectId()}

	// Test1
	num, err := DB.GetCount(&item1)
	if err != nil {
		t.Errorf("issues pulling count from database")
	}

	if num != 0 {
		t.Errorf("Expected 0 got %v", num)
	}

	DB.CreateEntry(&item1)

	//item 2
	item2 := TestModel{"blah2", "emperor2", bson.NewObjectId()}
	DB.CreateEntry(&item2)

	num, err = DB.GetCount(&item1)
	if err != nil {
		t.Errorf("issues pulling count from database")
	}

	if num != 2 {
		t.Errorf("Expected 2 got %v", num)
	}
}

func TestDeleteEntry(t *testing.T) {

	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	//testcase 1
	tc := TestModel{Name: "Blah"}
	_, err = DB.CreateEntry(&tc)
	if err != nil {
		t.Fatalf("Issues inserting into database")
	}

	err = DB.DeleteEntry(&tc)
	if err != nil {
		t.Errorf("Unable to delete the model from the database")
	}

	//testcase 2 from just an id
	id := bson.NewObjectId()
	tc = TestModel{Name: "Blah", ID: id}
	idout, err := DB.CreateEntry(&tc)
	if err != nil {
		t.Fatalf("Issues inserting into database")
	}

	if idout.(bson.ObjectId) != id {
		t.Errorf("Manual insertion of ID did not work")
	}

	err = DB.DeleteEntry(&TestModel{ID: id})
	if err != nil {
		t.Errorf("Unable to delete the model from the database")
	}
}

func TestDeleteEntries(t *testing.T) {

	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	//clear the database collection TESTDATABASE
	DB.GetDb().C(TESTDATABASE).DropCollection()

	names := []string{"Blah", "Blarg", "Blaggo", "Opps"}
	ids := []bson.ObjectId{}

	//TESTCASE1
	var tc TestModel
	for _, name := range names {
		tc = TestModel{Name: name}
		id, err := DB.CreateEntry(&tc)
		if err != nil {
			t.Fatalf("Issues inserting into database")
		}
		ids = append(ids, id.(bson.ObjectId))
	}

	num, err := DB.GetCount(&tc)
	if err != nil {
		t.Errorf("Error getting count from the database")
	}

	numrm, err := DeleteEntries(DB.db, TESTDATABASE, ids)
	if err != nil {
		t.Errorf("Issues deleting entries from the database")
	}

	if num != numrm {
		t.Errorf("Expected count to be %v got %v", numrm, num)
	}

	//TESTCASE 2 - Empty collection
	ids = []bson.ObjectId{}
	numrm, err = DeleteEntries(DB.db, TESTDATABASE, ids)
	if err != nil {
		t.Errorf("Issues deleting entries from the database")
	}
	if numrm > 0 {
		t.Errorf("Expected removed to be 0 got %v", numrm)
	}

}

func TestEmptyCollection(t *testing.T) {
	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	tcs := []TestModel{
		TestModel{Name: "Drew"},
		TestModel{Name: "Dog"},
		TestModel{Name: "Cat"},
	}

	for _, tc := range tcs {
		DB.CreateEntry(&tc)
	}

	num, err := DB.EmptyCollection(&tcs[0])
	if err != nil {
		fmt.Println("Error resetting the model collection", tcs[0].GetModelName())
	}
	fmt.Printf("%v removed from the database\n", num)

	if num == 0 {
		t.Errorf("Unalbe to delete elements in the database:", num)
	}
}

func TestSelectAll(t *testing.T) {

	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	DB.EmptyCollection(&TestModel{})

	tcs := []TestModel{
		TestModel{Name: "Drew"},
		TestModel{Name: "Dog"},
		TestModel{Name: "Cat"},
	}

	for _, tc := range tcs {
		DB.CreateEntry(&tc)
	}

	out, err := DB.SelectAll(&TestModel{})
	if err != nil {
		t.Errorf("Unalbe to select elements from the database:", err)
	}
	list := out.(*[]*TestModel)
	if len(*list) != len(tcs) {
		t.Errorf("Expected length %v, got %v", len(tcs), len(*list))
	}
}

func TestFindEntries(t *testing.T) {

	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	DB.EmptyCollection(&TestModel{})

	tcs := []TestModel{
		TestModel{Name: "Drew", Title: "King"},
		TestModel{Name: "Dog", Title: "Peasant"},
		TestModel{Name: "Cat", Title: "Knight"},
		TestModel{Name: "Cat", Title: "Peasant"},
	}
	for _, tc := range tcs {
		DB.CreateEntry(&tc)
	}

	//Test 1
	list := make(map[string]interface{})
	list["name"] = "Cat"
	out, err := DB.FindEntries(&TestModel{}, list)
	if err != nil {
		t.Error("Issues pulling entries from the database")
	}

	num := len(*out.(*[]*TestModel))
	if num != 2 {
		t.Errorf("Expected number found, %w to be equal to 2", num)
	}

	//Test 2
	list = make(map[string]interface{})
	list["name"] = "Cat"
	list["title"] = "Peasant"
	out, err = DB.FindEntries(&TestModel{}, list)
	if err != nil {
		t.Error("Issues pulling entries from the database")
	}
	num = len(*out.(*[]*TestModel))
	if num != 1 {
		t.Errorf("Expected number found, %w to be equal to 1", num)
	}

	//Test 3
	list = make(map[string]interface{})
	list["title"] = "Peasant"
	out, err = DB.FindEntries(&TestModel{}, list)
	if err != nil {
		t.Error("Issues pulling entries from the database")
	}
	num = len(*out.(*[]*TestModel))
	if num != 2 {
		t.Errorf("Expected number found, %w to be equal to 2", num)
	}
}

func TestUpdateEntry(t *testing.T) {
	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	DB.EmptyCollection(&TestModel{})

	tcs := []TestModel{
		TestModel{Name: "Drew", Title: "King"},
		TestModel{Name: "Dog", Title: "Peasant"},
		TestModel{Name: "Cat", Title: "Knight"},
		TestModel{Name: "Cat", Title: "Peasant"},
	}
	for _, tc := range tcs {
		DB.CreateEntry(&tc)
	}

	//Test 1

	listtoupdate := make(map[string]interface{})
	listtoupdate["name"] = "Drew"
	updates := make(map[string]interface{})
	updates["title"] = "master"
	num, err := DB.UpdateEntry(&TestModel{}, listtoupdate, updates)
	if err != nil {
		t.Errorf("Error providing updates to the database")
	}

	if num != 1 {
		t.Errorf("Expected number of updates, %v. to be 1", num)
	}

	//Test 2

	listtoupdate = make(map[string]interface{})
	listtoupdate["name"] = "Cat"
	updates = make(map[string]interface{})
	updates["title"] = "animals"
	num, err = DB.UpdateEntry(&TestModel{}, listtoupdate, updates)
	if err != nil {
		t.Errorf("Error providing updates to the database")
	}

	if num != 2 {
		t.Errorf("Expected number of updates, %v. to be 1", num)
	}

	//Test 3

	listtoupdate = make(map[string]interface{})
	listtoupdate["name"] = "Blarg"
	updates = make(map[string]interface{})
	updates["title"] = "master"
	num, err = DB.UpdateEntry(&TestModel{}, listtoupdate, updates)
	if err != nil {
		t.Errorf("Error providing updates to the database")
	}

	if num != 0 {
		t.Errorf("Expected number of updates, %v. to be 1", num)
	}

	//Test 4

	listtoupdate = make(map[string]interface{})
	listtoupdate["name"] = "Blarg"
	updates = make(map[string]interface{})
	num, err = DB.UpdateEntry(&TestModel{}, listtoupdate, updates)
	if err != nil {
		t.Errorf("Error providing updates to the database")
	}

	if num != 0 {
		t.Errorf("Expected number of updates, %v. to be 1", num)
	}
}

func TestFindEntry(t *testing.T) {
	DB, err := NewMongoDb(testconfigservername, testconfigdbname)
	if err != nil {
		fmt.Println("Unable to connect to Database: ", err)
		t.Errorf("Issues connecting to the database")
	}

	DB.EmptyCollection(&TestModel{})

	tc := TestModel{Name: "Drew", Title: "King"}
	_, err = DB.CreateEntry(&tc)
	if err != nil {
		t.Fatal("Issues creating the database entry")
	}

	//test case 1
	entry, err := DB.FindEntry(&tc)
	if err != nil {
		t.Error("Error retreiving data from the database")
	}

	entry_ := **entry.(**TestModel)
	if entry_.Name != tc.Name {
		t.Errorf("Expected %v, got %v", tc.Name, entry_.Name)
	}

	//test case 2
	tc = TestModel{}
	err = tc.SetModelID(bson.NewObjectId())
	if err != nil {
		t.Errorf("Issues setting the id")
	}

	entry, err = DB.FindEntry(&tc)
	if err == nil {
		t.Error("Error should not retreive data from the database")
	}

}
