package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/logging"
	"projects/lampoon/services/nodes/models"
	"strconv"

	"github.com/gorilla/mux"
)

//ASSETFILENAME : name of the current file for logging purposes
const ASSETFILENAME = "asset_controller.go"

//AssetController is the instance created by the constructor. Has a number of methods to handle the
//incoming http requests
type AssetController struct {
	tpl *template.Template
}

//NewAssetController is the constructor for the asset controllers
func NewAssetController() *AssetController {
	tpl := template.Must(template.ParseGlob(os.Getenv("LAMPOONPATH") + "/templates/asset/*"))
	return &AssetController{tpl}
}

//Index returns all of the assets as a json
func (trc AssetController) Index(w http.ResponseWriter, r *http.Request) {

	m := models.EmptyAsset()
	vu, err := m.Index()
	if err != nil {
		msg := "Error retrieving all of the asset data"
		logging.Error(msg, ASSETFILENAME+"-Index", err)
		config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, msg)
	}

	uj, _ := json.Marshal(vu)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

//CreateForm is used in template mode to create an asset
func (trc AssetController) CreateForm(w http.ResponseWriter, req *http.Request) {
	if config.Options.TemplateMode == "true" {
		u := models.EmptyAsset()
		trc.tpl.ExecuteTemplate(w, "create.gohtml", u)
	} else {
		w.WriteHeader(http.StatusForbidden)
		return
	}
}

//Create an asset, json format, "name"
func (trc AssetController) Create(w http.ResponseWriter, req *http.Request) {

	rp := models.EmptyAsset()

	if config.Options.TemplateMode == "true" {
		name := req.FormValue("name")
		rp.Name = name
		domain, _ := strconv.Atoi(req.FormValue("domain"))
		rp.Domain = domain
		tp, _ := strconv.Atoi(req.FormValue("type"))
		rp.Type = tp
		description := req.FormValue("description")
		rp.Description = description
	} else {
		err := rp.ParseReqBody(req.Body)
		if err != nil {
			logging.Error("Error parsing body", ASSETFILENAME+"-Create", err)
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, err.Error(), rp)
		}
	}

	logging.DebugMessage("Parsed values from the request body", ASSETFILENAME+"-Create")
	logging.DebugMessage(fmt.Sprintf("name: %v", rp.Name), ASSETFILENAME+"-Create")

	_, err := rp.Create()
	if err != nil {
		errmsg := fmt.Sprintf("assests, action Create: Error creating entry: %v", rp.Name)
		logging.Error(errmsg, ASSETFILENAME+"-Create", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/assets", http.StatusSeeOther)
		return
	} else {
		js, _ := json.Marshal(rp)
		w.WriteHeader(http.StatusCreated) // 200
		fmt.Fprint(w, string(js), "\n")
	}
}

//Delete an asset from the database, need to give "id"
func (trc AssetController) Delete(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]
	m := models.EmptyAsset()
	m.SetModelID(id)
	err := m.Delete()
	if err != nil {
		errmsg := fmt.Sprintf("assests, action Delete: Error creating entry: %v", id)
		logging.Error(errmsg, ASSETFILENAME+"-Delete", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/assets", http.StatusSeeOther)
		return
	} else {
		w.WriteHeader(http.StatusAccepted)
	}
}

//Show returns a single asset (json), requires "id"
func (trc AssetController) Show(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]

	asst := models.EmptyAsset()
	asst.SetModelID(id)
	fullasst, err := asst.FindMe()
	if err != nil {
		errmsg := fmt.Sprintf("assests, action Show: Error finding entry: %v", id)
		logging.Error(errmsg, ASSETFILENAME+"-Show", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	js, _ := json.Marshal(fullasst)
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprint(w, string(js), "\n")
}

//UpdateForm is used in template mode to update an asset
func (trc AssetController) UpdateForm(w http.ResponseWriter, req *http.Request) {

	vars := mux.Vars(req)
	id := vars["id"]

	asst := models.EmptyAsset()
	asst.SetModelID(id)
	_, err := asst.FindMe()
	if err != nil {
		errmsg := fmt.Sprintf("assests, action UpdateForm: Error finding entry: %v", id)
		logging.Error(errmsg, ASSETFILENAME+"-UpdateForm", err)
		config.ResErrorEmptyReqPayload(w, http.StatusNotFound, errmsg)
		return
	}

	if config.Options.TemplateMode == "true" {
		u := models.EmptyAsset()
		trc.tpl.ExecuteTemplate(w, "update.gohtml", u)
	} else {
		w.WriteHeader(http.StatusForbidden)
		return
	}
}

//Update an asset, requires id and name in json
func (trc AssetController) Update(w http.ResponseWriter, req *http.Request) {

	rp := models.EmptyAsset()
	if config.Options.TemplateMode == "true" {
		name := req.FormValue("name")
		rp.Name = name
	} else {
		err := rp.ParseReqBody(req.Body)
		if err != nil {
			logging.Error("Unable to parse request body", ASSETFILENAME+"-Update", err)
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, err.Error(), rp)
			return
		}
	}

	vars := mux.Vars(req)
	id := vars["id"]
	rp.SetModelID(id)
	logging.Info(fmt.Sprintf("%#v", rp), ASSETFILENAME+"-Update")

	err := rp.UpdateMe()
	if err != nil {
		errmsg := fmt.Sprintf("assests, action Update: Error creating entry: %v", id)
		logging.Error(errmsg, ASSETFILENAME+"-Update", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/assets", http.StatusSeeOther)
		return
	} else {
		js, _ := json.Marshal(rp)
		w.WriteHeader(http.StatusAccepted)
		fmt.Fprint(w, string(js), "\n")
	}
}
