package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/logging"
	"projects/lampoon/services/nodes/models"
	"time"
)

const SESSIONFILENAME = "session_controller.go"

type SessionController struct {
	tpl *template.Template
}

//NewSessionController is the contstruct for the session object
func NewSessionController() *SessionController {
	tpl := template.Must(template.ParseGlob(os.Getenv("LAMPOONPATH") + "/templates/session/*"))
	return &SessionController{tpl}
}

//Create - post route for resource session
func (sc SessionController) Create(w http.ResponseWriter, req *http.Request) {

	// vars := mux.Vars(req)

	sm := models.NewSession()

	err := sm.ParseReqBody(req.Body)
	if err != nil {
		logging.Error("Error parsing body", SESSIONFILENAME+"-Create", err)
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, err.Error(), sm)
		return
	}

	vals, err := sm.ValidateToken()
	if err != nil {
		msg := "Token is not valid"
		logging.Error(msg, SESSIONFILENAME+"-Create", err)
		config.ResErrorEmptyReqPayload(w, http.StatusUnauthorized, msg)
		return
	}

	userid := vals.Sub

	user, err := models.FindUser(userid)
	if err != nil {
		u, err := models.CreateUser(userid)
		if err != nil {
			errmsg := fmt.Sprintf("sessions, action Create: Error pulling user data from the database %v", err)
			config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
			return
		}
		w.WriteHeader(http.StatusCreated)
		uj, _ := json.Marshal(u)
		fmt.Fprintf(w, "%s\n", uj)
		return
	}

	w.WriteHeader(http.StatusAccepted)
	uj, _ := json.Marshal(user)
	fmt.Fprintf(w, "%s\n", uj)

	user.LastLogin = int(time.Now().Unix())
	err = models.UpdateUser(user)
	if err != nil {
		errmsg := fmt.Sprintf("sessions, action Create: Error pulling user data from the database %v", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	return
}

//Delete is the logout
func (sc SessionController) Delete(w http.ResponseWriter, req *http.Request) {
	w.WriteHeader(http.StatusNoContent)
	// uj, _ := json.Marshal(user)
	fmt.Fprint(w)
}

func AlreadyLoggedIn(req *http.Request) (bool, *models.User) {

	sm := models.NewSession()
	err := sm.ParseReqBody(req.Body)
	if err != nil {
		logging.Error("Error parsing body", SESSIONFILENAME+"-Create", err)
		return false, nil
	}

	vals, err := sm.ValidateToken()
	if err != nil {
		msg := "Token is not valid"
		logging.Error(msg, SESSIONFILENAME+"-Create", err)
		return false, nil
	}

	user, err := models.FindUser(vals.Sub)
	if err != nil {
		msg := "Token is not valid"
		logging.Error(msg, SESSIONFILENAME+"-Create", err)
		return false, nil
	}

	return true, user
}
