package controllers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/models"
	"testing"
)

func TestLinkCreate(t *testing.T) {

	config.Options.TemplateMode = "false"
	lc := NewLinkController()
	//create body for request

	ast1 := models.EmptyAsset()
	ast2 := models.EmptyAsset()
	ast1.SetModelID("5a03b5b363b28b30403eb6b1")
	ast2.SetModelID("5a03cc1863b28b45311a7d94")
	ast1.Name = "Dog"
	ast2.Name = "Cat"
	ast1.Create()
	ast2.Create()

	ast2.UpdateMe()

	//Case1 2 different nodes
	//Case2 nodes are the same
	//Case3 one of the nodes doesn't exist in database

	tt := []struct {
		Data struct {
			From string `json:"from"`
			To   string `json:"to"`
		}
		StatusCode int
	}{
		{struct {
			From string `json:"from"`
			To   string `json:"to"`
		}{"5a03b5b363b28b30403eb6b1", "5a03cc1863b28b45311a7d94"}, http.StatusCreated},
		{struct {
			From string `json:"from"`
			To   string `json:"to"`
		}{"5a03b5b363b28b30403eb6b1", "5a03b5b363b28b30403eb6b1"}, http.StatusBadRequest},
		{struct {
			From string `json:"from"`
			To   string `json:"to"`
		}{"5a03b5b363b38b30403eb6b1", "5a03b5b363b28b30403eb6b1"}, http.StatusBadRequest},
	}

	for _, tc := range tt {
		m, _ := json.Marshal(tc.Data)

		req, err := http.NewRequest("POST", "localhost:8085/api/links", bytes.NewBuffer(m))
		if err != nil {
			t.Fatalf("Could not create created request: %v", err)

		}
		rec := httptest.NewRecorder()

		lc.Create(rec, req)

		res := rec.Result()
		defer res.Body.Close()
		if res.StatusCode != tc.StatusCode {
			t.Errorf("expected status %v; go %v", tc.StatusCode, res.StatusCode)
		}
	}

	//Clear database
	ast := models.EmptyAsset()
	ast.DeleteAll()
}
