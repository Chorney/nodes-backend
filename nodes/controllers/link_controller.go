package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/logging"
	"projects/lampoon/services/nodes/models"

	"gopkg.in/mgo.v2/bson"

	"github.com/gorilla/mux"
)

//LinkFILENAME : name of the current file for logging purposes
const LINKFILENAME = "link_controller.go"

//LinkController is the instance created by the constructor. Has a number of methods to handle the
//incoming http requests
type LinkController struct {
	tpl *template.Template
}

//NewLinkController is the constructor for the Link controllers
func NewLinkController() *LinkController {
	tpl := template.Must(template.ParseGlob(os.Getenv("LAMPOONPATH") + "/templates/link/*"))
	return &LinkController{tpl}
}

//Index returns all of the Links as a json
func (trc LinkController) Index(w http.ResponseWriter, r *http.Request) {

	m := models.EmptyLink()
	vu, err := m.Index()
	if err != nil {
		msg := "Error retrieving all of the Link data"
		logging.Error(msg, LINKFILENAME+"-Index", err)
		config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, msg)
	}

	uj, _ := json.Marshal(vu)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

//CreateForm is used in template mode to create an Link
func (trc LinkController) CreateForm(w http.ResponseWriter, req *http.Request) {
	if config.Options.TemplateMode == "true" {
		u := models.EmptyLink()
		trc.tpl.ExecuteTemplate(w, "create.gohtml", u)
	} else {
		w.WriteHeader(http.StatusForbidden)
		return
	}
}

//Create an Link, json format, "name"
func (trc LinkController) Create(w http.ResponseWriter, req *http.Request) {

	rp := models.EmptyLink()

	if config.Options.TemplateMode == "true" {
		from := req.FormValue("from")
		to := req.FormValue("to")
		rp.Nodes = []bson.ObjectId{bson.ObjectIdHex(from), bson.ObjectIdHex(to)}
	} else {
		err := rp.ParseReqBody(req.Body)
		if err != nil {
			logging.Error("Error parsing body", LINKFILENAME+"-Create", err)
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, err.Error(), rp)
		}
	}

	logging.DebugMessage("Parsed values from the request body", LINKFILENAME+"-Create")

	_, err := rp.Create()
	if err != nil {
		errmsg := fmt.Sprintf("links, action Create: Error creating entry: %v", rp.Nodes)
		logging.Error(errmsg, LINKFILENAME+"-Create", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/links", http.StatusSeeOther)
		return
	} else {
		js, _ := json.Marshal(rp)
		w.WriteHeader(http.StatusCreated) // 200
		fmt.Fprint(w, string(js), "\n")
	}
}

//Delete an Link from the database, need to give "id"
func (trc LinkController) Delete(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]
	m := models.EmptyLink()
	m.SetModelID(id)
	err := m.Delete()
	if err != nil {
		errmsg := fmt.Sprintf("assests, action Delete: Error creating entry: %v", id)
		logging.Error(errmsg, LINKFILENAME+"-Delete", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/links", http.StatusSeeOther)
		return
	} else {
		w.WriteHeader(http.StatusAccepted)
	}
}

//GetLinksForNode returns all the links for a given node, input = bson.ObjectId
func (trc LinkController) GetLinksForNode(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]
	asst := models.EmptyAsset()
	asst.SetModelID(id)

	links, err := models.GetLinksForNode(asst)
	if err != nil {
		errmsg := fmt.Sprintf("assests, action GetLinksForNode: Error finding links for node_id: %v", id)
		logging.Error(errmsg, ASSETFILENAME+"-GetLinksForNOde", err)
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	js, _ := json.Marshal(links)
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprint(w, string(js), "\n")
}
