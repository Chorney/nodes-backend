package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/helpers"
	"projects/lampoon/services/nodes/models"
	"testing"

	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"
)

func TestAssetCreate(t *testing.T) {

	config.Options.TemplateMode = "false"
	ac := NewAssetController()
	//create body for request

	tt := []struct {
		Data struct {
			Name string `json:"name"`
		}
		StatusCode int
	}{
		{struct {
			Name string `json:"name"`
		}{"neo"}, http.StatusCreated},
		{struct {
			Name string `json:"name"`
		}{""}, http.StatusBadRequest},
	}

	for _, tc := range tt {
		m, _ := json.Marshal(tc.Data)

		req, err := http.NewRequest("POST", "localhost:8085/api/assets/create", bytes.NewBuffer(m))
		if err != nil {
			t.Fatalf("Could not create created request: %v", err)

		}
		rec := httptest.NewRecorder()

		ac.Create(rec, req)

		res := rec.Result()
		defer res.Body.Close()
		if res.StatusCode != tc.StatusCode {
			t.Errorf("expected status %v; go %v", tc.StatusCode, res.StatusCode)
		}
	}
}

func TestAssetIndex(t *testing.T) {

	ac := NewAssetController()

	ast := models.EmptyAsset()
	ast.DeleteAll()

	astnames := []string{"Jim", "Jeff", "Jill"}
	for _, name := range astnames {
		a := models.NewAsset(name)
		_, err := a.Create()
		if err != nil {
			t.Error("Unable to create test case instance")
		}
	}

	req, err := http.NewRequest("GET", "localhost:8085/api/assets/index", nil)
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)

	}
	rec := httptest.NewRecorder()

	ac.Index(rec, req)

	res := rec.Result()
	if res.StatusCode != 200 {
		t.Errorf("Expected 200, got %v", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()
	if err != nil {
		t.Error("Issues parsin the response body")
	}

	assts := []models.Asset{}
	err = json.Unmarshal(body, &assts)
	if err != nil {
		t.Errorf("Issues unmarshalling the response json into go struct")
	}

	if len(assts) == 0 {
		t.Errorf("Expected length of json array to be %v, not %v", len(astnames), len(assts))
	}

	for _, ast := range assts {
		if ok, _ := helpers.InArray(ast.Name, astnames); !ok {
			t.Errorf("Expected %v, to be in the list of jsons", ast.Name)
		}
	}
}

func TestAssetDelete(t *testing.T) {

	config.Options.TemplateMode = "false"

	ac := NewAssetController()
	ast := models.EmptyAsset()
	ast.DeleteAll()

	name := "Drew"
	asst := models.NewAsset(name)
	id, err := asst.Create()
	if err != nil {
		t.Fatalf("Issues creating a model element")
	}

	//case 1
	url := fmt.Sprintf("localhost:8085/api/assets/delete/%v", "123123123")
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)
	}
	rec := httptest.NewRecorder()

	ac.Delete(rec, req)

	res := rec.Result()
	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	// //case 2
	// need to test through the mux, using httptest.NewServer
	r := mux.NewRouter()
	r.HandleFunc("/api/assets/delete/{id}", ac.Delete).Methods("DELETE")

	srv := httptest.NewServer(r)
	defer srv.Close()

	url = fmt.Sprintf("%v/api/assets/delete/%v", srv.URL, id.Hex())
	req, err = http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)
	}

	res, err = http.DefaultClient.Do(req)

	if res.StatusCode != http.StatusAccepted {
		t.Errorf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

}

func TestAssetShow(t *testing.T) {

	ac := NewAssetController()
	ast := models.EmptyAsset()
	ast.DeleteAll()

	r := mux.NewRouter()
	r.HandleFunc("/api/assets/show/{id}", ac.Show).Methods("GET")

	srv := httptest.NewServer(r)
	defer srv.Close()

	name := "Drew"
	asst := models.NewAsset(name)
	id, err := asst.Create()
	if err != nil {
		t.Fatalf("Issues creating a model element")
	}

	//case 1
	url := fmt.Sprintf("%v/api/assets/show/%v", srv.URL, id.Hex())
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)
	}

	res, err := http.DefaultClient.Do(req)

	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	emptasst := models.EmptyAsset()
	emptasst.ParseReqBody(res.Body)
	if emptasst.Name != asst.Name {
		t.Errorf("Expected Name from show to be %v got %v", asst.Name, emptasst.Name)
	}

	//case 2
	url = fmt.Sprintf("%v/api/assets/show/%v", srv.URL, bson.NewObjectId().Hex())
	req, err = http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)
	}

	res, err = http.DefaultClient.Do(req)

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

}

func TestAssetUpdate(t *testing.T) {

	config.Options.TemplateMode = "false"

	ac := NewAssetController()
	ast := models.EmptyAsset()
	ast.DeleteAll()

	r := mux.NewRouter()
	r.HandleFunc("/api/assets/update/{id}", ac.Update).Methods("PUT")

	srv := httptest.NewServer(r)
	defer srv.Close()

	name := "Drew"
	asst := models.NewAsset(name)
	id, err := asst.Create()
	if err != nil {
		t.Fatalf("Issues creating a model element")
	}

	//case 1
	url := fmt.Sprintf("%v/api/assets/update/%v", srv.URL, id.Hex())

	tc := struct {
		Name string `json:"name"`
	}{"neo"}

	m, _ := json.Marshal(tc)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(m))
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)
	}

	res, err := http.DefaultClient.Do(req)

	if res.StatusCode != http.StatusAccepted {
		t.Errorf("Expected %v, got %v", http.StatusAccepted, res.StatusCode)
	}

	emptasst := models.EmptyAsset()
	emptasst.ParseReqBody(res.Body)
	if emptasst.Name != tc.Name {
		t.Errorf("Expected Name from show to be %v got %v", tc.Name, emptasst.Name)
	}

	//case 2
	url = fmt.Sprintf("%v/api/assets/update/%v", srv.URL, bson.NewObjectId().Hex())

	req, err = http.NewRequest("PUT", url, nil)
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)
	}

	res, err = http.DefaultClient.Do(req)

	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("Expected %v, got %v", http.StatusBadRequest, res.StatusCode)
	}

	//Clear database
	ast = models.EmptyAsset()
	ast.DeleteAll()

}
