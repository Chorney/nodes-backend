package models

import (
	"projects/lampoon/services/nodes/database"
)

// type db = database.Db

type Db interface {
	SelectAll(database.Model) (interface{}, error)
	CreateEntry(database.Model) (interface{}, error)
	DeleteEntry(database.Model) error
	EmptyCollection(database.Model) (int, error)
	FindEntries(database.Model, map[string]interface{}) (interface{}, error)
	UpdateEntry(database.Model, map[string]interface{}, map[string]interface{}) (int, error)
	FindEntry(database.Model) (interface{}, error)
}

// type RestfulM interface {
// 	database.Model
// 	Index() (interface{}, error)
// 	Create() error
// 	// Get(param)
// 	// Delete()
// 	// Create()
// 	// Update()
// }

type ModelConfig struct {
	ModelName string
}

func (m *ModelConfig) GetModelName() string {
	return m.ModelName
}
