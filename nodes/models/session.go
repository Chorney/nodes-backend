package models

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/database"
	"projects/lampoon/services/nodes/logging"
	"strconv"
	"time"

	mgo "gopkg.in/mgo.v2"
)

type DbCheckResult int

const EXISTS DbCheckResult = 1
const EXPIRED DbCheckResult = 2
const DNE DbCheckResult = 3

//LINKFILENAME used for error logging
const SESSIONFILENAME = "session.go"
const SESSIONMODELNAME = "session"

var DbSessions *map[string]Session

type Session struct {
	IdToken string `json:"id_token" bson:"id_token"`
	db      *mgo.Database
}

//IdTokenCheck stuct used to marshal the json data from token check @ google
type IdTokenCheck struct {
	Iss string `json:"iss" bson:"iss"`
	Sub string `json:"sub" bson:"sub"`
	Azp string `json:"azp" bson:"azp"`
	Aud string `json:"aud" bson:"aud"`
	Iat string `json:"iat" bson:"iat"`
	Exp string `json:"exp" bson:"exp"`
}

func NewSession() *Session {
	dbb, err := database.DialDbDirect(config.Config.Server, config.Config.Dbname)
	if err != nil {
		logging.Error("Error connecting to the database", SESSIONFILENAME+"-EmptyLink", err)
	}
	return &Session{db: dbb}
}

func (s *Session) ParseReqBody(body io.ReadCloser) error {

	b, err := ioutil.ReadAll(body)
	defer body.Close()

	if err != nil {
		errmsg := "assets,  Error reading request body"
		logging.Error(errmsg, SESSIONFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}

	var ma NodesMarshaller
	err = json.Unmarshal(b, &ma)
	if err != nil {
		errmsg := "assets,  Error unmarshalling request body from json"
		logging.Error(errmsg, SESSIONFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}

	return nil
}

//PopEndPointData will make a call to google endpoint and unmrshall data into session structure
func (s *Session) PopEndPointData() (*IdTokenCheck, error) {

	tokenchk := s.IdToken

	blah := IdTokenCheck{}

	//DO GET REQUEST TO:
	res, err := http.Get(fmt.Sprintf("https: //www.googleapis.com/oauth2/v3/tokeninfo?id_token=%v", tokenchk))
	if err != nil {
		errmsg := "Error with request to google tokenid endpoint check"
		logging.Error(errmsg, SESSIONFILENAME+"-ValidateToken", err)
		return nil, fmt.Errorf("%v", errmsg)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		errmsg := "Error reading response from google tokenid endpoint check"
		logging.Error(errmsg, SESSIONFILENAME+"-ValidateToken", err)
		return nil, fmt.Errorf("%v", errmsg)
	}

	err = json.Unmarshal(body, &blah)
	if err != nil {
		errmsg := "Error unmarshalling data into Sessino structure"
		logging.Error(errmsg, SESSIONFILENAME+"-ValidateToken", err)
		return nil, fmt.Errorf("%v", errmsg)
	}

	return &blah, nil
}

//ValidateTokenFromEndPoint does validation by checking the google endpoint returns userid
func (s *Session) ValidateTokenFromEndPoint() (*IdTokenCheck, error) {

	vals, err := s.PopEndPointData()
	if err != nil {
		return nil, err
	}

	if vals.Iss != "accounts.google.com" && vals.Iss != "https://accounts.google.com" {
		return nil, fmt.Errorf("Wrong issuer")
	}

	//  If auth request is from a G Suite domain:
	//  if idinfo['hd'] != GSUITE_DOMAIN_NAME:
	//  raise ValueError('Wrong hosted domain.')

	//  ID token is valid. Get the user's Google Account ID from the decoded token.
	// userid := vals.Sub

	return vals, nil
}

//ValidateToken validates using the built in library
func (s *Session) ValidateToken() (*IdTokenCheck, error) {
	//TODO: now it is just a mirror of the endpoint version

	// this code needs to be fixed, its just a copy of ValidateTokenFromEndPoint

	vals, err := s.PopEndPointData()
	if err != nil {
		return nil, err
	}

	if vals.Iss != "accounts.google.com" && vals.Iss != "https://accounts.google.com" {
		return nil, fmt.Errorf("Wrong issuer")
	}

	if vals.Aud != "988419272895-mn8t466j4rjta4ncq6887njd8ca4jog4.apps.googleusercontent.com" {
		return nil, fmt.Errorf("Wrong issuer")
	}

	//  If auth request is from a G Suite domain:
	//  if idinfo['hd'] != GSUITE_DOMAIN_NAME:
	//  raise ValueError('Wrong hosted domain.')
	timestamp := int(time.Now().Unix())
	expiry, err := strconv.Atoi(vals.Exp)
	if err != nil {
		return nil, err
	}
	if expiry <= timestamp {
		return nil, fmt.Errorf("Token Expired")
	}

	//  ID token is valid. Get the user's Google Account ID from the decoded token.
	// userid := vals.Sub

	return vals, nil
}

//CheckTokenDb looks in the database for an entry with id_token
func (s *Session) CheckTokenDb() DbCheckResult {

	list := make(map[string]interface{})
	list["id_token"] = s.IdToken

	err := s.db.C("session").Find(list).One(s)
	if err != nil {
		msg := fmt.Sprintf("Error finding elements in the database: %+v", list)
		logging.Notice(msg, SESSIONFILENAME+"-CheckTokenDb")
		return DNE
	}

	_, err = s.ValidateTokenFromEndPoint()
	if err != nil {
		msg := fmt.Sprintf("Token is not valid: %+v", list)
		logging.Notice(msg, SESSIONFILENAME+"-CheckTokenDb")
		return EXPIRED
	}
	return EXISTS
}
