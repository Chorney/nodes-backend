package models

import (
	"fmt"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/database"
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestLinkCreate(t *testing.T) {

	// connect to the real database, eventually will need to mock it out
	db, err := database.NewMongoDb(config.Config.Server, config.Config.Dbname)
	if err != nil {
		t.Fatal("Unable to connect to database")
	}

	asst1 := NewAsset("Drew")
	asst2 := NewAsset("Chorney")
	id1, err := asst1.Create()
	if err != nil {
		t.Errorf("Issues with creating assets")
	}
	id2, err := asst2.Create()
	if err != nil {
		t.Errorf("Issues with creating assets")
	}

	tt := []struct {
		name1 bson.ObjectId
		name2 bson.ObjectId
		err   error
	}{
		{name1: bson.NewObjectId(), name2: bson.NewObjectId(), err: fmt.Errorf("One or more nodes not in the database")},
		{name1: id1, name2: id2, err: nil},
		{name1: id1, name2: bson.NewObjectId(), err: fmt.Errorf("One or more nodes not in the database")},
		{name1: bson.NewObjectId(), name2: id2, err: fmt.Errorf("One or more nodes not in the database")},
	}

	for _, tc := range tt {
		tcl := NewLink(tc.name1, tc.name2)
		linkid, err := tcl.Create()

		if err == nil {
			if err != tc.err {
				t.Errorf("Expected error %v, but got %v", tc.err, err)
			}
		} else {
			if err.Error() != tc.err.Error() {
				t.Errorf("Expected error %v, but got %v", tc.err, err)
			}
		}

		// Clean up the database, as we populated into the realdatabase
		if err == nil {
			err = db.GetDb().C("link").Remove(bson.M{"_id": linkid})
			if err != nil {
				fmt.Println(err)
				t.Errorf("Issue's cleaning up the created links from the database")
			}
		}
	}

	err = asst1.Delete()
	if err != nil {
		t.Errorf("Could not delete asset from database")
	}
	err = asst2.Delete()
	if err != nil {
		t.Errorf("Could not delete asset from database")
	}
}

func TestDeleteLink(t *testing.T) {
	name1 := "Drew"
	name2 := "Chorney"
	tc1 := NewAsset(name1)
	tc2 := NewAsset(name2)
	// fmt.Println("--------------------------")
	// // fmt.Printf("%#v", tc2)
	_, err := tc1.Create()
	if err != nil {
		t.Error("Issue inserting the document into the model")
	}
	_, err = tc2.Create()
	if err != nil {
		t.Error("Issue inserting the document into the model")
	}

	l1 := NewLink(tc1.GetModelID(), tc2.GetModelID())
	_, err = l1.Create()
	if err != nil {
		t.Errorf("Issues creating link")
	}

	err = l1.Delete()
	if err != nil {
		t.Errorf("Issues deleting link from the database")
	}

	tc1.Delete()
	tc2.Delete()
}

func TestGetLinksForNode(t *testing.T) {

	asst1 := NewAsset("Drew")
	asst2 := NewAsset("Robert")
	asst3 := NewAsset("Chorney")
	asst4 := NewAsset("Blah")

	id1, err := asst1.Create()
	if err != nil {
		t.Errorf("Issues with creating assets")
	}
	id2, err := asst2.Create()
	if err != nil {
		t.Errorf("Issues with creating assets")
	}
	id3, err := asst3.Create()
	if err != nil {
		t.Errorf("Issues with creating assets")
	}
	id4, err := asst4.Create()
	if err != nil {
		t.Errorf("Issues with creating assets")
	}

	l1 := NewLink(id1, id2)
	_, err = l1.Create()
	if err != nil {
		t.Errorf("Issues creating link")
	}
	l2 := NewLink(id1, id3)
	_, err = l2.Create()
	if err != nil {
		t.Errorf("Issues creating link")
	}
	l3 := NewLink(id2, id3)
	_, err = l3.Create()
	if err != nil {
		t.Errorf("Issues creating link")
	}
	l4 := NewLink(id1, id4)
	_, err = l4.Create()
	if err != nil {
		t.Errorf("Issues creating link")
	}

	//Test 1
	links, err := GetLinksForNode(asst1)
	numlinks := len(links)

	if numlinks != 3 {
		t.Errorf("Expected 3 links got %v", numlinks)
	}

	//Test 2
	asst5 := NewAsset("Not in database")
	links, err = GetLinksForNode(asst5)
	numlinks = len(links)

	if numlinks != 0 {
		t.Errorf("Expected 0 links got %v", numlinks)
	}

	//Test 3
	links, err = GetLinksForNode(asst4)
	numlinks = len(links)

	if numlinks != 1 {
		t.Errorf("Expected 1 links got %v", numlinks)
	}

	//CLEAN UP

	err = asst1.Delete()
	if err != nil {
		t.Errorf("Could not delete asset from database")
	}
	err = asst2.Delete()
	if err != nil {
		t.Errorf("Could not delete asset from database")
	}
	err = asst3.Delete()
	if err != nil {
		t.Errorf("Could not delete asset from database")
	}
	err = asst4.Delete()
	if err != nil {
		t.Errorf("Could not delete asset from database")
	}

	l1.Delete()
	l2.Delete()
	l3.Delete()
	l4.Delete()
}
