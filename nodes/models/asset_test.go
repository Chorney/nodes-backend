package models

import (
	"fmt"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/database"
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestAssetCreate(t *testing.T) {

	name := "Drew"
	tc2 := NewAsset(name)
	// fmt.Println("--------------------------")
	// // fmt.Printf("%#v", tc2)
	_, err := tc2.Create()
	if err != nil {
		t.Error("Issue inserting the document into the model")
	}

	db, err := database.NewMongoDb(config.Config.Server, config.Config.Dbname)
	if err != nil {
		t.Fatal("Unable to connect to database")
	}
	cnt, err := db.GetDb().C("asset").Find(bson.M{"name": name}).Count()
	if err != nil {
		t.Error("Issues counting the number of documents in the asset collection")
	}
	if cnt == 0 {
		t.Errorf("Expected count in the asset collection to be greater than 0 got: %v", cnt)
	}

	newid := tc2.ID
	err = db.GetDb().C("asset").Remove(bson.M{"_id": newid})
	if err != nil {
		fmt.Println("Error deleting test document from database")
	}
}

func TestAssetDelete(t *testing.T) {

	temp := NewAsset("")
	temp.DeleteAll()

	name := "Drew"
	tc1 := NewAsset(name)
	// fmt.Println("--------------------------")
	// // fmt.Printf("%#v", tc2)
	id1, err := tc1.Create()
	if err != nil {
		t.Error("Issue inserting the document into the model")
	}

	name = "Jill"
	tc2 := NewAsset(name)
	// fmt.Println("--------------------------")
	// // fmt.Printf("%#v", tc2)
	id2, err := tc2.Create()
	if err != nil {
		t.Error("Issue inserting the document into the model")
	}

	l := NewLink(id1, id2)
	_, err = l.Create()
	if err != nil {
		t.Errorf("Error creating new link")
	}

	links, err := GetLinksForNode(tc1)
	if err != nil {
		t.Errorf("Trouble getting links for node: %v", err)
	}

	if len(links) != 1 {
		t.Errorf("Expected 1 link not %v", len(links))
	}

	err = tc2.Delete()
	if err != nil {
		t.Errorf("unable to delete asset from database")
	}

	links, err = GetLinksForNode(tc1)
	if err != nil {
		t.Errorf("Trouble getting links for node: %v", err)
	}

	if len(links) != 0 {
		t.Errorf("Expected 0 link not %v", len(links))
	}

}

func TestAssetDeleteAll(t *testing.T) {
	tcn := []string{"Drew", "John", "Jill"}

	var tcs []*Asset
	for _, tn := range tcn {
		tcs = append(tcs, NewAsset(tn))
	}

	for _, tc := range tcs {
		_, err := tc.Create()
		if err != nil {
			t.Error("Issue inserting the document into the model")
		}
	}

	temp := NewAsset("")
	err := temp.DeleteAll()
	if err != nil {
		t.Error("Issues delete the collection")
	}
}

func TestAssetIndex(t *testing.T) {

	tcn := []string{"Drew", "John", "Jill"}

	var tcs []*Asset
	for _, tn := range tcn {
		tcs = append(tcs, NewAsset(tn))
	}

	temp := NewAsset("")
	temp.DeleteAll()

	for _, tc := range tcs {
		_, err := tc.Create()
		if err != nil {
			t.Error("Issue inserting the document into the model")
		}
	}

	asts, err := tcs[0].Index()
	if err != nil {
		t.Error("Testing: AssetIndex. Issues retreiving the assets from the database")
	}

	for i, ast := range asts {
		fmt.Println(i)
		fmt.Println(ast)
		if ast.Name != tcn[i] {
			t.Errorf("Model namee returnd from Index, %v, does not match the testcase names: %v", ast.Name, tcn[i])
		}
	}

	temp.DeleteAll()

}

func TestAssetFind(t *testing.T) {

	//case 1
	find_map := make(map[string]interface{})
	find_map["name"] = "Drew"
	ast := EmptyAsset()
	ast.DeleteAll()

	tcn := []string{"Drew", "John", "Jill", "Drew"}

	var tcs []*Asset
	for _, tn := range tcn {
		tcs = append(tcs, NewAsset(tn))
	}

	for _, tc := range tcs {
		_, err := tc.Create()
		if err != nil {
			t.Error("Issue inserting the document into the model")
		}
	}

	asts, err := ast.Find(find_map)
	if err != nil {
		fmt.Println("Error finding items from database")
	}

	num := len(asts)
	if num != 2 {
		t.Errorf("Expected number returned, %v, to be equal to %v", num, 2)
	}

	//case 2
	find_map = make(map[string]interface{})
	find_map["name"] = "Drew"
	find_map["_id"] = tcs[0].GetModelID()

	asts, err = ast.Find(find_map)
	if err != nil {
		fmt.Println("Error finding items from database")
	}

	num = len(asts)
	if num != 1 {
		t.Errorf("Expected number returned, %v, to be equal to %v", num, 1)
	}
	ast.DeleteAll()
}

func TestAssetUpdate(t *testing.T) {

	ast := EmptyAsset()
	ast.DeleteAll()

	tcn := []string{"Drew", "John", "Jill", "Drew"}

	var tcs []*Asset
	for _, tn := range tcn {
		tcs = append(tcs, NewAsset(tn))
	}

	for _, tc := range tcs {
		_, err := tc.Create()
		if err != nil {
			t.Error("Issue inserting the document into the model")
		}
	}

	//case 1
	find_map := make(map[string]interface{})
	find_map["name"] = "Drew"
	updates := make(map[string]interface{})
	updates["name"] = "Taco"

	num, err := ast.Update(find_map, updates)
	if err != nil {
		t.Error("Unable to update the database")
	}

	if num != 2 {
		t.Errorf("Expected number of updates to be 2 not %v", num)
	}

	//case 2
	find_map = make(map[string]interface{})
	find_map["name"] = "Taco"
	updates = make(map[string]interface{})
	updates["name"] = "Drew"

	num, err = ast.Update(find_map, updates)
	if err != nil {
		t.Error("Unable to update the database")
	}

	if num != 2 {
		t.Errorf("Expected number of updates to be 2 not %v", num)
	}

	//case 3
	find_map = make(map[string]interface{})
	find_map["name"] = "Drew"
	updates = make(map[string]interface{})
	updates["hair"] = "brown"

	num, err = ast.Update(find_map, updates)
	if err != nil {
		t.Error("Unable to update the database")
	}

	if num != 0 {
		t.Errorf("Expected number of updates to be 0 not %v", num)
	}

	//case 4
	find_map = make(map[string]interface{})
	find_map["name"] = "Jill"
	updates = make(map[string]interface{})
	updates["name"] = "Jessa"

	num, err = ast.Update(find_map, updates)
	if err != nil {
		t.Error("Unable to update the database")
	}

	if num != 1 {
		t.Errorf("Expected number of updates to be 1 not %v", num)
	}
}

func TestAssetUpdateMe(t *testing.T) {
	ast := EmptyAsset()
	ast.DeleteAll()

	name := "Drew"
	asst := NewAsset(name)
	_, err := asst.Create()
	if err != nil {
		t.Fatal("Unalbe to create test entyr in the database")
	}

	//case 1
	asst.Name = "Jill"

	_ = asst.UpdateableAttrs()

	asst.UpdateMe()

	if asst.Name != "Jill" {
		t.Errorf("Expected Jill, got %v", asst.Name)
	}

	find_map := make(map[string]interface{})
	find_map["name"] = "Jill"
	a, err := asst.Find(find_map)
	if err != nil {
		t.Error("unable to retreive any items from the database")
	}

	if a[0].Name != "Jill" {
		t.Errorf("Expected Jill, got %v", a[0].Name)
	}

	asst.Delete()

}
