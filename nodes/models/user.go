package models

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/database"
	"projects/lampoon/services/nodes/logging"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

//LINKFILENAME used for error logging
const USERFILENAME = "user.go"
const USERMODELNAME = "user"

type User struct {
	Sub            string `json:"sub" bson:"sub"`
	LastLogin      int    `json:"last_login" bson:"last_login"`
	AccountCreated int    `json:"account_created" bson:"account_created"`
	db             *mgo.Database
}

//IdTokenCheck stuct used to marshal the json data from token check @ google

func NewUser() *User {
	dbb, err := database.DialDbDirect(config.Config.Server, config.Config.Dbname)
	if err != nil {
		logging.Error("Error connecting to the database", USERFILENAME+"-EmptyLink", err)
	}
	return &User{db: dbb}
}

func (u *User) ParseReqBody(body io.ReadCloser) error {

	b, err := ioutil.ReadAll(body)
	defer body.Close()

	if err != nil {
		errmsg := "assets,  Error reading request body"
		logging.Error(errmsg, USERFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}

	err = json.Unmarshal(b, u)
	if err != nil {
		errmsg := "assets,  Error unmarshalling request body from json"
		logging.Error(errmsg, USERFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}

	return nil
}

//FindUser finds a user in the mongodb
func FindUser(userid string) (*User, error) {

	u := NewUser()
	list := make(map[string]interface{})
	list["sub"] = userid

	err := u.db.C("user").Find(list).One(&u)
	if err != nil {
		msg := fmt.Sprintf("Error finding elements in the database: %+v", list)
		logging.Notice(msg, USERFILENAME+"-FindUser")
		return nil, err
	}

	return u, nil
}

//CreateUser inserts a user into the mongodb
func CreateUser(userid string) (*User, error) {

	u := NewUser()
	u.Sub = userid
	u.LastLogin = -1
	u.AccountCreated = int(time.Now().Unix())

	err := u.db.C("user").Insert(u)
	if err != nil {
		msg := fmt.Sprintf("Could not create user in the database: %+v", u)
		logging.Error(msg, USERFILENAME+"-CreateUser", err)
		return nil, err
	}

	return u, nil
}

//UpdateUser updates the user with new attributes
func UpdateUser(user *User) error {

	findlist := make(map[string]interface{})

	findlist["sub"] = user.Sub

	err := user.db.C("user").Update(findlist, user)
	if err != nil {
		msg := fmt.Sprintf("Error finding user in the database: %+v", findlist)
		logging.Error(msg, USERFILENAME+"-UpdateUser", err)
		return err
	}

	return nil
}

func DeleteUser(user *User) error {

	err := user.db.C("user").Remove(bson.M{"sub": user.Sub})
	if err != nil {
		msg := fmt.Sprintf("Error finding user in the database: %+v", user)
		logging.Error(msg, USERFILENAME+"-DeleteUser", err)
		return err
	}
	return nil
}
