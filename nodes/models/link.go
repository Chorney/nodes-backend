package models

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/database"
	"projects/lampoon/services/nodes/logging"
	"reflect"

	"gopkg.in/mgo.v2/bson"
)

//LINKFILENAME used for error logging
const LINKFILENAME = "link.go"
const LINKMODELNAME = "link"

type Link struct {
	ID    bson.ObjectId   `json:"id" bson:"_id"`
	Nodes []bson.ObjectId `json:"nodes" bson:"_nodes"`
	// From               bson.ObjectId `json:"from" bson:"node_id"`
	// To                 bson.ObjectId `json:"to" bson:"node_id"`
	ModelConfig `json:"-" bson:"-"`
	db          Db
}

func EmptyLink() *Link {
	dbb, err := database.NewMongoDb(config.Config.Server, config.Config.Dbname)
	if err != nil {
		logging.Error("Error connecting to the database", LINKFILENAME+"-EmptyLink", err)
	}
	return &Link{ModelConfig: ModelConfig{LINKMODELNAME}, db: dbb}
}

func NewLink(from bson.ObjectId, to bson.ObjectId) *Link {
	dbb, err := database.NewMongoDb(config.Config.Server, config.Config.Dbname)
	if err != nil {
		logging.Error("Error connecting to the database", LINKFILENAME+"-NewAsset", err)
	}

	nodes := []bson.ObjectId{from, to}

	return &Link{Nodes: nodes, ModelConfig: ModelConfig{LINKMODELNAME}, db: dbb}
}

//Basical model implementation to meet the requirements of the mongo database drivers

func (m *Link) SetModelID(id interface{}) error {

	switch id.(type) {
	case int:
		tempint := int(reflect.ValueOf(id).Int())
		inthex := fmt.Sprintf("%x", tempint)
		m.ID = bson.ObjectIdHex(inthex)
	case string:
		temp := reflect.ValueOf(id).String()
		if bson.IsObjectIdHex(temp) != true {
			return fmt.Errorf("Id is not hex")
		}
		m.ID = bson.ObjectIdHex(temp)
	case bson.ObjectId:
		m.ID = id.(bson.ObjectId)
	default:
		return fmt.Errorf("Id is not of a relevant type")
	}
	return nil
}

//GetModelID returns the model id
func (m *Link) GetModelID() bson.ObjectId {
	return m.ID
}

//CRUD COMMANDS

func (m *Link) Index() ([]*Link, error) {
	a, err := m.db.SelectAll(m)
	links := a.(*[]*Link)
	return *links, err
}

//Create inserts model into the database
func (m *Link) Create() (bson.ObjectId, error) {

	nodes, err := m.GetNodes()
	if err != nil {
		logging.Error("Error retreiving link nodes", LINKFILENAME+"-Create", err)
		return m.GetModelID(), fmt.Errorf("One or more nodes not in the database")
	}

	ast1 := EmptyAsset()
	ast2 := EmptyAsset()
	ast1.SetModelID(nodes[0])
	ast2.SetModelID(nodes[1])

	idcheck1, _ := ast1.FindMe()
	idcheck2, _ := ast2.FindMe()

	if idcheck1 != nil && idcheck2 != nil && idcheck1.GetModelID() != idcheck2.GetModelID() {
		id, err := m.db.CreateEntry(m)
		if err != nil {
			logging.Error("Error with creating entry", LINKFILENAME+"-Create", err)
			return m.GetModelID(), fmt.Errorf("One or more nodes not in the database")
		}
		return id.(bson.ObjectId), nil
	}
	return m.GetModelID(), fmt.Errorf("One or more nodes not in the database")
}

//Delete a link from database
func (m *Link) Delete() error {
	err := m.db.DeleteEntry(m)
	if err != nil {
		logging.Error(fmt.Sprintf("error deleting model from the database: %v", m.GetModelID()), LINKFILENAME+"-Delete", err)
		return err
	}
	return nil
}

//DeleteAll links from the database
func (m *Link) DeleteAll() error {
	_, err := m.db.EmptyCollection(m)
	if err != nil {
		logging.Error("Error emptying the collection", LINKFILENAME+"-DeleteAll", err)
		return err
	}
	return nil
}

//GetNodes returns the two nodes in the link
func (m *Link) GetNodes() ([]bson.ObjectId, error) {

	if len(m.Nodes) == 0 {
		return m.Nodes, fmt.Errorf("There are no nodes in the link")
	}
	return m.Nodes, nil
}

//NodesMarshaller is for unpacking with create json for links
type NodesMarshaller struct {
	From string `json: "from"`
	To   string `json: "to"`
}

//ParseReqBody parses body and then stores it in the model
func (m *Link) ParseReqBody(body io.ReadCloser) error {

	b, err := ioutil.ReadAll(body)
	defer body.Close()

	if err != nil {
		errmsg := "assets,  Error reading request body"
		logging.Error(errmsg, LINKFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}

	var ma NodesMarshaller
	err = json.Unmarshal(b, &ma)
	if err != nil {
		errmsg := "assets,  Error unmarshalling request body from json"
		logging.Error(errmsg, LINKFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}
	m.SetNodes(&ma)

	return nil
}

//SetNodes sets the nodes of the link using the values from and to obtained from the
//NodesMarhsaller
func (m *Link) SetNodes(nodes *NodesMarshaller) {
	m.Nodes = []bson.ObjectId{bson.ObjectIdHex(nodes.From), bson.ObjectIdHex(nodes.To)}
}

//**// MODEL METHODS THAT TALK DIRECT TO DATABASE

//GetLinksForNode returns all the links that are in common with an input node
func GetLinksForNode(ast *Asset) ([]*Link, error) {

	DB, err := database.DialDbDirect(config.Config.Server, config.Config.Dbname)
	if err != nil {
		logging.Emergency("Error dialing the database", LINKFILENAME+"-GetLinksForNode", err)
	}

	id := ast.GetModelID()
	links := []*Link{}

	err = DB.C(LINKMODELNAME).Find(bson.M{"_nodes": bson.M{"$elemMatch": bson.M{"$in": []bson.ObjectId{id}}}}).All(&links)
	if err != nil {
		logging.Error("Trouble finding elements in the database", LINKFILENAME+"-SelectAll", err)
	}

	return links, err
}
