package models

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"projects/lampoon/services/nodes/config"
	"projects/lampoon/services/nodes/database"
	"projects/lampoon/services/nodes/helpers"
	"projects/lampoon/services/nodes/logging"
	"reflect"

	"gopkg.in/mgo.v2/bson"
)

//ASSETFILENAME used for error logging
const ASSETFILENAME = "asset.go"
const ASSETMODELNAME = "asset"

type Asset struct {
	ID          bson.ObjectId `json:"id" bson:"_id"`
	Name        string        `json:"name" bson:"name"`
	Domain      int           `json:"domain" bson:"domain"`
	Type        int           `json:"type" bson:"type"`
	Description string        `json:"description" bson:"description"`
	ModelConfig `json:"-" bson:"-"`
	db          Db
}

func EmptyAsset() *Asset {
	dbb, err := database.NewMongoDb(config.Config.Server, config.Config.Dbname)
	if err != nil {
		logging.Error("Error connecting to the database", ASSETFILENAME+"-EmptyAsset", err)
	}
	return &Asset{ModelConfig: ModelConfig{ASSETMODELNAME}, db: dbb}
}

func NewAsset(name string) *Asset {
	dbb, err := database.NewMongoDb(config.Config.Server, config.Config.Dbname)
	if err != nil {
		logging.Error("Error connecting to the database", ASSETFILENAME+"-NewAsset", err)
	}
	return &Asset{Name: name, ModelConfig: ModelConfig{ASSETMODELNAME}, db: dbb}
}

//Basical model implementation to meet the requirements of the mongo database drivers

func (m *Asset) SetModelID(id interface{}) error {

	switch id.(type) {
	case int:
		tempint := int(reflect.ValueOf(id).Int())
		inthex := fmt.Sprintf("%x", tempint)
		m.ID = bson.ObjectIdHex(inthex)
	case string:
		temp := reflect.ValueOf(id).String()
		if bson.IsObjectIdHex(temp) != true {
			return fmt.Errorf("Id is not hex")
		}
		m.ID = bson.ObjectIdHex(temp)
	case bson.ObjectId:
		m.ID = id.(bson.ObjectId)
	default:
		return fmt.Errorf("Id is not of a relevant type")
	}
	return nil
}

//GetModelID returns the model id
func (m *Asset) GetModelID() bson.ObjectId {
	return m.ID
}

//Index returns all the assets
func (m *Asset) Index() ([]*Asset, error) {
	a, err := m.db.SelectAll(m)
	assets := a.(*[]*Asset)
	return *assets, err
}

//Create inserts model into the database
func (m *Asset) Create() (bson.ObjectId, error) {

	if m.Name == "" {
		return m.ID, fmt.Errorf("Asset Name cannot be the empty string")
	}

	id, err := m.db.CreateEntry(m)
	if err != nil {
		logging.Error("Error with creating entry", ASSETFILENAME+"-Create", err)
	}
	return id.(bson.ObjectId), err
}

//DeleteAll removes everthing in the database of type model
func (m *Asset) DeleteAll() error {
	_, err := m.db.EmptyCollection(m)

	//if emptying all assets need to nuke links
	link := EmptyLink()
	link.db.EmptyCollection(link)

	if err != nil {
		logging.Error("Error emptying the collection", ASSETFILENAME+"-DeleteAll", err)
		return err
	}
	return nil
}

//Delete removes model from the database
func (m *Asset) Delete() error {
	err := m.db.DeleteEntry(m)
	if err != nil {
		logging.Error(fmt.Sprintf("error deleting model from the database: %v", m.GetModelID()), ASSETFILENAME+"-Delete", err)
		return err
	}

	links, err := GetLinksForNode(m)
	if err != nil {
		logging.Notice("Error finding links for the given node", ASSETFILENAME+"-Delete")
	}

	ids := helpers.ListModelsToIds(links)

	DB, _ := database.DialDbDirect(config.Config.Server, config.Config.Dbname)
	num, err := database.DeleteEntries(DB, "link", ids)
	if err != nil {
		logging.Notice("NO links deleted from the database", ASSETFILENAME+"-Delete")
	}
	logging.Notice(fmt.Sprintf("%v links deleted from the database", num), ASSETFILENAME+"-Delete")

	return nil
}

//Find assets
func (m *Asset) Find(list map[string]interface{}) ([]*Asset, error) {

	//filter out any names in the list map that are not in the model fields
	filterlist := m.FilterList(list)

	a, err := m.db.FindEntries(m, filterlist)
	if err != nil {
		logging.Error("Error finding entries", ASSETFILENAME+"-DeleteAll", err)
	}
	assets := a.(*[]*Asset)
	return *assets, err
}

//FindMe returns the present model asset
func (m *Asset) FindMe() (*Asset, error) {
	a, err := m.db.FindEntry(m)
	if a != nil {
		return *a.(**Asset), err
	}
	return nil, err
}

//Update the model
func (m *Asset) Update(list map[string]interface{}, updates map[string]interface{}) (int, error) {

	filterlist := m.FilterList(list)
	updatesfilter := m.FilterList(updates)

	num, err := m.db.UpdateEntry(m, filterlist, updatesfilter)
	if err != nil {
		logging.Error("Error updating the database from models", ASSETFILENAME+"-Update", err)
	}

	return num, err
}

//UpdateMe updates the database entry
func (m *Asset) UpdateMe() error {

	findlist := make(map[string]interface{})
	findlist["_id"] = m.GetModelID()

	updates := m.UpdateableAttrs()

	_, err := m.db.UpdateEntry(m, findlist, updates)
	if err != nil {
		logging.Error("Unable to update the model", ASSETFILENAME+"-UpdateMe", err)
	}
	return nil
}

//------------------------- Model helper functions -------------------------------------

//UpdateableAttrs returns a map of updateable attributes
func (m *Asset) UpdateableAttrs() map[string]interface{} {

	mnames := make(map[string]interface{})
	val := reflect.ValueOf(m).Elem()
	typ := val.Type()
	for i := 0; i < val.NumField(); i++ {
		// fmt.Println(typ.Field(i).Name)
		if typ.Field(i).Name != "db" && typ.Field(i).Name != "ModelConfig" && typ.Field(i).Name != "ID" {
			if val.Field(i).Interface() != 0 && val.Field(i).Interface() != "" {
				mnames[typ.Field(i).Tag.Get("bson")] = val.Field(i).Interface()
			}
		}
	}
	return mnames
}

//FilterList filter out any names in the list map that are not in the model fields
func (m *Asset) FilterList(list map[string]interface{}) map[string]interface{} {

	mnames := make(map[string]int)
	val := reflect.ValueOf(m).Elem()
	typ := val.Type()
	for i := 0; i < val.NumField(); i++ {
		// fmt.Println(typ.Field(i).Name)
		mnames[typ.Field(i).Tag.Get("bson")] = 1
	}

	for k := range list {
		if _, ok := mnames[k]; !ok {
			delete(list, k)
		}
	}
	if _, ok := list["db"]; ok {
		delete(list, "db")
	}
	if _, ok := list["ModelConfig"]; ok {
		delete(list, "ModelConfig")
	}

	return list
}

//ParseReqBody parses body and then stores it in the model
func (m *Asset) ParseReqBody(body io.ReadCloser) error {

	b, err := ioutil.ReadAll(body)
	defer body.Close()

	if err != nil {
		errmsg := "assets,  Error reading request body"
		logging.Error(errmsg, ASSETFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}

	err = json.Unmarshal(b, m)
	if err != nil {
		errmsg := "assets,  Error unmarshalling request body from json"
		logging.Error(errmsg, ASSETFILENAME+"-ParseReqBody", err)
		return fmt.Errorf("%v", errmsg)
	}
	return nil
}
