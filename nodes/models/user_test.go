package models

import (
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestCreateDeleteUser(t *testing.T) {

	id := "1234"

	insuser, err := CreateUser(id)
	if err != nil {
		t.Errorf("Issue creating user in the database,%v", err)
	}

	num_db, err := insuser.db.C("user").Find(bson.M{"sub": id}).Count()
	if err != nil {
		t.Errorf("Issue creating user in the database,%v", err)
	}

	if num_db != 1 {
		t.Errorf("Expected 1 element in the database, got %v", num_db)
	}

	err = DeleteUser(insuser)
	if err != nil {
		t.Errorf("Issues deleting user from the database, %v", err)
	}

	num_db, err = insuser.db.C("user").Find(bson.M{"sub": id}).Count()
	if err != nil {
		t.Errorf("Issue creating user in the database,%v", err)
	}

	if num_db != 0 {
		t.Errorf("Expected 1 element in the database, got %v", num_db)
	}

}

func TestFindUser(t *testing.T) {

	id := "1234"

	insuser, err := CreateUser(id)
	if err != nil {
		t.Errorf("Issue creating user in the database,%v", err)
	}

	inuser_check, err := FindUser(id)
	if err != nil {
		t.Errorf("Issue creating user in the database,%v", err)
	}

	if insuser.Sub != inuser_check.Sub {
		t.Errorf("Created user not found in database, %+v,.... %+v", insuser, inuser_check)
	}

	err = DeleteUser(insuser)
	if err != nil {
		t.Errorf("Issues deleting user from the database, %v", err)
	}

}

//func TestDuplicateUser(t *testing.T) {
//	id := "1234"
//	insuser, err := CreateUser(id)
//	if err != nil {
//		t.Errorf("Issue creating user in the database,%v", err)
//	}
//
//	_, err = CreateUser(id)
//	if err == nil {
//		t.Errorf("There should be a duplication error creating user in the database")
//	}
//
//	err = DeleteUser(insuser)
//	if err != nil {
//		t.Errorf("Issues deleting user from the database, %v", err)
//	}
//}
