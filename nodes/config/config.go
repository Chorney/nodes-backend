package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
)

var Options FlagConfig

type FlagConfig struct {
	ProductionFlag string
	TemplateMode   string
	UnlockedMode   string
	DebugMode      string
	Verbosity      int
	DecLocMsg      string
	FullErrMsg     string
}

var Config Configuration
var Once sync.Once

type Configuration struct {
	Server string
	Dbname string
}

func init() {
	processFlags()
	Once.Do(func() {
		cfg := ParseConfig()
		Config = cfg
	})
}

func ParseConfig() Configuration {
	var err error
	var raw []byte
	// if Options.ProductionFlag == "true" {
	// 	raw, err = ioutil.ReadFile("/opt/config.json")
	// } else {
	raw, err = ioutil.ReadFile(os.Getenv("LAMPOONPATH") + "/config.json")
	// }
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	c := Configuration{}
	json.Unmarshal(raw, &c)

	return c
}

func processFlags() {
	flag.StringVar(&Options.ProductionFlag, "production", "false", "When in production mode uses other config file variables (default false)")
	flag.StringVar(&Options.TemplateMode, "tplmode", "true", "Template mode means you can only log in by sending a post, and a number of other options")
	flag.StringVar(&Options.UnlockedMode, "unlockedmode", "false", "If unlocked mode == true than all pages are open, and no need to login, default is false")
	flag.StringVar(&Options.DebugMode, "debugmode", "true", "When debug mode is true, debug messages are printed to the terminal (default true)")
	flag.StringVar(&Options.DecLocMsg, "declocmsg", "true", "Includes a decorator for all messages to display where the message location is inside code (default true)")
	flag.StringVar(&Options.FullErrMsg, "fullerrmsg", "true", "Full error message reporting includes the string from the golang error (default false)")
	flag.IntVar(&Options.Verbosity, "verbosity", 5, "Levels of verbosity 0-5, default 5")
	flag.Parse()
}
