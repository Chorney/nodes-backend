FROM base/archlinux
RUN pacman -Syu --noconfirm && pacman --noconfirm -S mongodb go git
ADD /nodes /opt/nodes/
RUN go get github.com/gorilla/mux
RUN go get gopkg.in/mgo.v2
RUN go get gopkg.in/mgo.v2/bson
ENV LAMPOONPATH /go/src/projects/lampoon/services/nodes/
